package ru.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kitaev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("DEMO"));
        add(new Project("TEST"));
        add(new Project("MEGA"));
        add(new Project("BETA"));
    }

    public void create() {
        add(new Project("New Project" + System.currentTimeMillis()));
    }

    public void add(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull String id) {
        projects.remove(id);
    }

}
