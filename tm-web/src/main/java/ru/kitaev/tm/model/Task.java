package ru.kitaev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kitaev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    private Date dateFinish;

    private String projectId;

}
