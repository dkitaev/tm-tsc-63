package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kitaev.tm.api.service.dto.ISessionDTOService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;

public class SessionServiceTest {

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    private SessionDTO session;

    @NotNull
    @Autowired
    private IUserDTOService userService;

    @NotNull
    private final String userLogin = "test";

    @NotNull
    private final String userPassword = "test";

    public SessionServiceTest() {
        userService.create(userLogin, userPassword);
    }

    @Test
    @Category(UnitCategory.class)
    public void openTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    @Category(UnitCategory.class)
    public void closeTest() {
        session = sessionService.open(userLogin, userPassword);
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void validateTest() {
        @NotNull final SessionDTO session = sessionService.open("admin", "admin");
        sessionService.validate(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void validateRoleTest() {
        @NotNull final SessionDTO session = sessionService.open("admin", "admin");
        sessionService.validate(session, Role.ADMIN);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

}
