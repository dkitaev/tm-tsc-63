package ru.tsc.kitaev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.dto.ProjectDTO;

@Repository
public interface ProjectDTORepository extends AbstractOwnerDTORepository<ProjectDTO> {

    @NotNull
    ProjectDTO findByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

}
